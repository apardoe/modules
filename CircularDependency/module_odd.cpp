import module_zero;
import module_odd;
import module_even;

int Odd::one() { return zero() + 1; }

int Odd::three() {
    Even* even = new Even();
    return even->two() +one();
}
