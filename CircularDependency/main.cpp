#include "zero.h"

import std.core;
import module_odd;
import module_even;

int main() {
    Odd odd;
    Even even;
    std::cout << MESSAGE << odd.one() + even.two() + odd.three() + even.four() << std::endl;
    return 0;
}
