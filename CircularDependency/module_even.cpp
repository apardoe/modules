import module_zero;
import module_even;
import module_odd;


int Even::two() { return zero() + 2; }

int Even::four() {
    Odd* odd = new Odd();
    return odd->one() * two() + two();
}

  